<?php
#CMS - CMS Made Simple
#(c)2004 by Ted Kulp (wishy@users.sf.net)
#This project's homepage is: http://cmsmadesimple.sf.net
#(c)2010 MLE by Alberto Benati (cms@xme.it)
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#$Id: function.lang.php 115 2009-08-04 16:42:12Z alby $

function smarty_cms_function_lang($params, &$smarty)
{
	global $gCms;
	global $hls, $hl;

	if(!isset($hls)) return '';

	$spacer=' ';
	if(isset($params['spacer'])) $spacer = $params['spacer'];

	$class='';
	if(isset($params['class'])) $class = 'class="'.$params['class'].'"';

	$text=false;
	if(isset($params['text'])) $text = true;

	$config =& $gCms->GetConfig();
	$contentops =& $gCms->GetContentOperations();
	$contentobj = $contentops->LoadContentFromId( $gCms->variables['content_id'] );
	if(!$contentobj) return '<!-- no $contentobj -->';
	$alias = $contentobj->Alias();
	$lang_ready = $contentobj->mLangReady;
	$arr_mle=array();

	if(isset($params['nocurrent']))
	{
		$i = array_search($hl, $lang_ready);
		if($i !== false) unset($lang_ready[$i]);
	}

	$base_url = $config['root_url'];
	if($contentobj->Secure())
	{
		if(isset($config['ssl_url']))
		{
			$base_url = $config['ssl_url'];
		}
		else
		{
			$base_url = str_replace('http://','https://',$base_url);
		}
	}

	if($config['url_rewriting'] == 'mod_rewrite')
	{
		if($contentobj->DefaultContent() && get_site_preference('root_url_default_content',0))
		{
			$_url = $base_url.'/?hl=';
		}
		else
		{
			$_end_url = $contentobj->HierarchyPath().(isset($config['page_extension'])?$config['page_extension']:'.html');
		}
		foreach($hls as $key=>$mle)
		{
			if(!in_array($key, $lang_ready)) continue;
			$arr_mle[$key] = $mle;
			if($hl == $key) $arr_mle[$key]['current'] = true;
			$arr_mle[$key]['link'] = (isset($_end_url)) ? $base_url.'/'.$key.'/'.$_end_url : $_url.$key;
		}
	}
	else
	{
		if(isset($_SERVER['PHP_SELF']) && $config['url_rewriting'] == 'internal')
		{
			if($contentobj->DefaultContent() && get_site_preference('root_url_default_content',0))
			{
				$_url = $base_url.'/?hl=';
			}
			else
			{
				$_end_url = $contentobj->HierarchyPath().(isset($config['page_extension'])?$config['page_extension']:'.html');
			}
			foreach($hls as $key=>$mle)
			{
				if(!in_array($key, $lang_ready)) continue;
				$arr_mle[$key] = $mle;
				if($hl == $key) $arr_mle[$key]['current'] = true;
				$arr_mle[$key]['link'] = (isset($_end_url)) ? $base_url.'/index.php/'.$key.'/'.$_end_url : $_url.$key;
			}
		}
		else
		{
			if($contentobj->DefaultContent() && get_site_preference('root_url_default_content',0))
			{
				$_url = $base_url.'/?hl=';
			}
			else
			{
				$_url = $base_url.'/index.php?'.$config['query_var'].'='.$alias.'&amp;hl=';
			}
			foreach($hls as $key=>$mle)
			{
				if(!in_array($key, $lang_ready)) continue;
				$arr_mle[$key] = $mle;
				if($hl == $key) $arr_mle[$key]['current'] = true;
				$arr_mle[$key]['link'] = (!empty($params['urlparam'])) ? $_url.$key.$params['urlparam'] : $_url.$key;
			}
		}
	}

	if(isset($params['assign']))
	{
		$smarty->assign($params['assign'], $arr_mle);
		return '';
	}
	else
	{
		$selector='';
		foreach($arr_mle as $key=>$mle)
		{
			$link = ($text)?$mle['text']:$mle['flag'];
			if(isset($mle['current'])) $selector .= "<span xml:lang=\"{$mle['parent']}\" lang=\"{$mle['parent']}\" $class>$link</span>$spacer";
			else $selector .= "<a href=\"{$mle['link']}\" xml:lang=\"{$mle['parent']}\" lang=\"{$mle['parent']}\" $class>$link</a>$spacer";
		}
		if($spacer != '')
		{
			$length = strlen($spacer);
			$selector = substr($selector, 0, -1*($length));
		}
		return $selector;
	}
}

function smarty_cms_help_function_lang() {
?>
	<h3>What does this do?</h3>
	<p>This tag is used to insert language selector in into your template or page.
	It adds an extra language parameter to the current URL.</p>
	<h3>How do I use it?</h3>
	<p>This is just a basic tag plugin. You would insert it into your template or page like so: <code>{lang}</code>
	<h3>What parameters does it take?</h3>
	<p>spacer (optional) - delimiter between flags/texts. For instance, you can add <tt>spacer="&lt;br /&gt;"</tt>
		to increase the distance between flags/texts. The default value is a space</p>
	<p>class (optional) - add a style CSS class to link. Default is no class attribute</p>
	<p>text (optional) - if set display text language. Print values of text language, check this in config_lang.php</p>
	<p>nocurrent (optional) - if set drop current text or icon language</p>
	<p>urlparam (optional) - on each lang link add a new static / dynamic param. NO in mod_rewrite OR pretty url.
		For instance, you can add <tt>urlparam="&amp;lid=$page_alias"</tt></p>
	<p>assign (optional) - if set assign to smarty array for later use</p>
<?php
}

function smarty_cms_about_function_lang() {
?>
	<p>Authors: joeli &lt;licp@hotmail.com&gt;, katon &lt;katonchik@gmail.com&gt;</p>
	<p>Version 1.1 and above: Alberto Benati &lt;cms@xme.it&gt;</p>
	<p>
	Change History:<br/>
	1.1 - Compatibility with MLE 1.1.1 and above. Add spacer param<br />
	1.2 - Bugfix. Rewrite url generated<br />
	1.3 - Add urlparam (no mod_rewrite or pretty url), class and text params<br />
	1.4 - Wiedmann patch: enable custom 404 error with template, the current page is now not a link<br />
	1.5 - If config_lang is disabled you have official behavior<br />
	1.6 - View langs ready (only if default_lang_fallback isn't enabled) and add nocurrent param<br />
	1.7 - Add assign param, bugfix<br />
	1.8 - Add xml:lang and lang<br />
	1.9 - Update for secure<br />
	2.0 - Drop hierarchy (always true)
	</p>
<?php
}
?>