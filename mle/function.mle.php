<?php
#------------------------------------------------------------------------
# MLE - MultiLanguage Edition
# 2007 (c) alby (Alberto Benati) <cms@xme.it>
#------------------------------------------------------------------------
# CMS Made Simple is (c) 2005-2009 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#------------------------------------------------------------------------
#$Id: function.mle.php 113 2009-08-02 17:09:12Z alby $

function opacity_flags($replace, $percent, $reverse=false)
{
	global $hls, $hl;
	$percent = intval($percent);
	$strreplace = "filter:alpha(opacity={$percent});-moz-opacity:.{$percent};opacity:.{$percent};";
	foreach($hls as $key=>$val)
	{
		if($reverse)
		{
			$hls["$key"]['flag'] = str_replace($strreplace, $replace, $val['flag']);
			continue;
		}
		if($hl == $key) continue;
		if(strstr($val['flag'], $replace) !== false) $hls["$key"]['flag'] = str_replace($replace, $strreplace, $val['flag']);
	}
	return true;
}

function check_mle_pretty_urls($hls, $default='')
{
	$url = explode('/', $_SERVER['PHP_SELF']);
	foreach($url as $url_key=>$url_val)
	{
		foreach($hls as $key=>$val)
		{
			if($key == $url_val) return $key;
		}
	}
	return $default;
}


/*****************************************************
 Transparent Content Negociation - Modify by alby from
  www.php.net/source.php?url=/include/langchooser.inc
 *****************************************************/
//Order the array of compiled accept-language codes by quality value
function language_accept_order($a, $b)
{
	if($a[1] == $b[1]) return ($a[2] > $b[2]) ? 1 : -1;
	return ($a[1] > $b[1]) ? -1 : 1;
}

//Build a new array
function transform_language_arr( $hls )
{
	$t_hls=array('parent'=>array(), 'locale'=>array());
	foreach($hls as $key=>$val)
	{
		if(isset($val['locale_cms'])) $locale = $val['locale_cms'];
		else $locale = $key;

		if(isset($val['parent'])) $parent = strtolower($val['parent']);
		else $parent = strtolower($val['block']);
		$parent5 = strtolower(str_replace('_','-',$locale));

		if(!in_array($locale, array_keys($t_hls['locale']))) $t_hls['locale']["$locale"] = $key;
		if(!in_array($parent, array_keys($t_hls['parent']))) $t_hls['parent']["$parent"] = $key;
		if(!in_array($parent5, array_keys($t_hls['parent']))) $t_hls['parent']["$parent5"] = $key;
	}
	return $t_hls;
}

//Specified by the user via the browser's Accept Language setting
//Samples: "hu, en-us;q=0.66, en;q=0.33", "hu,en-us;q=0.5"
function language_user_setting( $hls )
{
	$browser_langs=array();
	$t_hls = transform_language_arr($hls);

	//Check if we have $_SERVER['HTTP_ACCEPT_LANGUAGE'] set and
	//it no longer breaks if you only have one language set :)
	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
	{
		$str = strtolower(trim($_SERVER["HTTP_ACCEPT_LANGUAGE"]));
		$str = str_replace(' ', '', $str);
		$browser_accept = explode(',', $str);

		//Go through all language preference specs
		for($i=0; $i<count($browser_accept); $i++)
		{
			//The language part is either a code or a code with a quality
			//We cannot do anything with a * code, so it is skipped
			//If the quality is missing, it is assumed to be 1 according to the RFC
			if(preg_match("!([a-z-]+)(;q=([0-9\\.]+))?!", $browser_accept[$i], $found))
			{
				$quality = (isset($found[3]) ? (float) $found[3] : 1.0);
				$browser_langs[] = array($found[1], $quality, $i+1);
			}
			unset($found);
		}
	}
	elseif(isset($_SERVER['HTTP_USER_AGENT']))
	{
		$str = strtolower(trim($_SERVER["HTTP_USER_AGENT"]));
		$user_agent = explode(';', $str);

		for($i=0; $i<sizeof($user_agent); $i++)
		{
			$languages = explode('-', $user_agent[$i]);
			if(sizeof($languages)==2 || strlen(trim($languages[0]))==2)
			{
				$browser_langs[] = array(trim($languages[0]), 1.0, $i+1);
			}
		}
	}

	//Sort by quality and order
	usort($browser_langs, "language_accept_order");

	foreach($browser_langs as $arr_language)
	{
		$language = $arr_language[0];
		if(in_array($language, array_keys($t_hls['parent']))) return $t_hls['parent']["$language"];
		if(strlen($language) > 2)
		{
			$language = substr($language, 0, 2);
			if(in_array($language, array_keys($t_hls['parent']))) return $t_hls['parent']["$language"];
		}
	}
	return DEFAULT_LANG;
}
?>