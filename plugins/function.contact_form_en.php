<?php

# CMSMS - CMS Made Simple
#
# (c)2004 by Ted Kulp (wishy@users.sf.net)
#
# This project's homepage is: http://cmsmadesimple.org
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function smarty_cms_function_contact_form_en($params, &$smarty) {

    global $gCms;

    if (FALSE == empty($params['captcha']) && $params['captcha'] && isset($gCms->modules['Captcha'])) 
    {
        $captcha =& $gCms->modules['Captcha']['object'];
    }

	if (empty($params['email'])){
		echo '<div class="formError">An email address must be specified in order to use this plugin.</div>';
		return;
	}else{
		$to = $params['email'];
	}
	
	$style = true; // Use default styles
	if (FALSE == empty($params['style']) && $params['style'] === "false" ) $style = false; // Except if "false" given in params
	
	// Default styles
	$inputStyle = 'style="width:95%;border: 1px solid #DADBDC; margin:0 0 1em 0;"'; // input boxes
	$taStyle = 'style="width:95%; border: 1px solid #DADBDC; margin:0 0 1em 0; color:#5A800C;"'; // TextArea boxes
	$formStyle = 'style="width:30em; important; font-weight: bold;"'; // form
	$errorsStyle = 'style="padding-left:20px; color: red; background-color: while; border: 1px solid #DADBDC;"'; // Errors box (div)
        $labelStyle = 'style="display:block; font-weight:bold;"';
        $buttonStyle = 'style="float:left; width:40%; border-right: 1px solid while; font-weight:normal; color: 87af0d; display:inline; padding:0px 5px; margin-right:10px;"';
        $fieldsetStyle = 'style="padding:1em;"';
        $captchaStyle = 'style="margin-bottom:1em; text-align: center;"';

	$errors=$name=$email=$subject=$message = '';
	if (FALSE == empty($params['subject_get_var']) && FALSE == empty($_GET[$params['subject_get_var']]))
	  {
	    $subject = $_GET[$params['subject_get_var']];
	  }
	if($_SERVER['REQUEST_METHOD']=='POST'){
		if (!empty($_POST['name'])) $name = cfSanitize($_POST['name']);
		if (!empty($_POST['email'])) $email = cfSanitize($_POST['email']);
		if (!empty($_POST['subject'])) $subject = cfSanitize($_POST['subject']);
		if (!empty($_POST['message'])) $message = $_POST['message'];
		if (FALSE == empty($params['captcha']) && $params['captcha'] && isset($gCms->modules['Captcha'])) 
		{
		    if (!empty($_POST['captcha_resp'])) { $captcha_resp = $_POST['captcha_resp']; }
		}

		//Mail headers
		$extra = "From: $name <$email>\r\n";
		$charset = isset($gCms->config['default_encoding']) && $gCms->config['default_encoding'] != '' ? $gCms->config['default_encoding'] : 'utf-8';
		$extra .= "Content-Type: text/plain; charset=" . $charset . "\r\n";
		
		if (empty($name)) $errors .= "\t\t<li>" . 'Vui lòng nhập họ và tên.' . "</li>\n";
		if (empty($email)) $errors .= "\t\t<li>" . 'Vui lòng nhập địa chỉ email.' . "</li>\n";
		elseif (!validEmail($email)) $errors .= "\t\t<li>" . 'Địa chỉ email không hợp lệ.' . "</li>\n";
		if (empty($subject)) $errors .= "\t\t<li>" . 'Vui lòng nhập tiêu đề.' . "</li>\n";
		if (empty($message)) $errors .= "\t\t<li>" . 'Vui lòng nhập nội dung.' . "</li>\n";
		if (FALSE == empty($params['captcha']) && $params['captcha'] && isset($gCms->modules['Captcha']))
		{
		    if (empty($captcha_resp)) $errors .= "\t\t<li>" . 'Please enter the text in the image' . "</li>\n";
		    elseif (! ($captcha->checkCaptcha($captcha_resp))) $errors .= "\t\t<li>" . 'The text from the image was not entered correctly' . "</li>\n";
		}
		
		if (!empty($errors)) {
			echo '<div class="formError" ' . (($style) ? $errorsStyle:'') . '>' . "\n";
			echo '<p>Thông báo lỗi! </p>' . "\n";
			echo "\t<ul>\n";
			echo $errors;
			echo "\t</ul>\n";
			echo "</div>";
		}
		elseif (@mail($to, $subject, $message . "\n\nsender: ".$_SERVER["REMOTE_ADDR"] . ', ' . $_SERVER["HTTP_USER_AGENT"] , $extra)) {
			echo '<div class="formMessage" style="color:red;">Chúc mừng bạn việc gửi liên hệ đã thành công!</div>' . "\n";
			return;
		}
		else {
			echo '<div class="formError" ' . (($style) ? $errorsStyle:'') . '>Xin lỗi, Việc gửi liên hệ thất bại. Có thể server đang lỗi!</div>' . "\n";
			return;
		}
	}

    if (isset($_SERVER['REQUEST_URI'])) 
    {
	$action = $_SERVER['REQUEST_URI'];
    }
    else
    {
	$action = isset($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : '';
	if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '') 
	{
	    $action .= '?'.$_SERVER['QUERY_STRING'];
	}
    }
?>

	<!-- CONTACT_FORM -->
	<form action="<?php echo $action ?>" method="post">	    
		<table border="0" width="100%" style="margin-bottom:15px;">        	
			<tr>
				<td style="width:20%" valign="top" style="padding-top:5px;"><strong>Full name</strong></td>
				<td valign="top" style="padding-top:5px;"><input type="text" id="name" name="name" value="<?php echo htmlspecialchars($name); ?>" <?php echo ($style) ? $inputStyle:''; ?>/></td>
			</tr>
			<tr>
				<td valign="top"><strong>Email</strong></td>
				<td valign="top"><input type="text" id="email" name="email" value="<?php echo htmlspecialchars($email); ?>" <?php echo ($style) ? $inputStyle:''; ?>/></td>
			</tr>
			<tr>
				<td valign="top"><strong>Subject</strong></td>
				<td valign="top"><input type="text" id="subject" name="subject" value="<?php echo htmlspecialchars($subject); ?>" <?php echo ($style) ? $inputStyle:''; ?>/></td>
			</tr>
			<tr>
				<td valign="top"><strong>Content</strong></td>
				<td valign="top"><textarea id="message" name="message" rows="5" cols="48" <?php echo ($style) ? $taStyle:''; ?>><?php echo $message; ?></textarea></td>
			</tr>
			<tr>
				<td valign="top"></td>
				<!--td valign="top">(<span style="font-weight:bold; font-size:13px; color:red;">*</span>)<span style="font-size:12px;"> Bắt buộc phải nhập tất cả các trường ở trên.</span></td-->
			</tr>
			<tr>
				<td valign="top"></td>
				<td style="margin-top:7px;" valign="top"><input type="submit" class="button" value="Send" <?php echo ($style) ? $buttonStyle: ''; ?> /> 
					<input type="reset"  class="button" value="Reset" <?php echo ($style) ? $buttonStyle: ''; ?> />  </td>
			</tr>						
		</table>			
	</form>
	<!-- END of CONTACT_FORM -->
<iframe width="700" height="650" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;ll=20.978533,105.830864&amp;spn=0.041113,0.073557&amp;iwloc=00049e1cf1bab9a06e797&amp;msid=201924081789481481185.00049e1cec0abe1b47229&amp;output=embed"></iframe><br /><small>View <a href="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;ll=20.978533,105.830864&amp;spn=0.041113,0.073557&amp;iwloc=00049e1cf1bab9a06e797&amp;msid=201924081789481481185.00049e1cec0abe1b47229&amp;source=embed" style="color:#0000FF;text-align:left">Hùng Gia</a> in a larger map</small>
<?php
}

function smarty_cms_help_function_contact_form_en() {
	?>
  <h2>NOTE: This plugin is deprecated</h2>
    <p>This smarty plugin is deprecated, and may not be included with further versions of CMS Made Simple.  We recommend you use the formbuilder module and it's included contact form.</p>
	<h3>What does this do?</h3>
	<p>Display's a contact form. This can be used to allow others to send an email message to the address specified.</p>
	<h3>How do I use it?</h3>
	<p>Just insert the tag into your template/page like: <code>{contact_form email="yourname@yourdomain.com"}</code><br>
	<br>
	If you would like to send an email to multiple adresses, seperate each address with a comma.</p>
	<h3>What parameters does it take?</h3>
	<ul>
		<li>email - The email address that the message will be sent to.</li>
		<li><em>(optional)</em>style - true/false, use the predefined styles. Default is true.</li>
		<li><em>(optional)</em>subject_get_var - string, allows you to specify which _GET var to use as the default value for subject.
               <p>Example:</p>
               <pre>{contact_form email="yourname@yourdomain.com" subject_get_var="subject"}</pre>
             <p>Then call the page with the form on it like this: /index.php?page=contact&subject=test+subject</p>
             <p>And the following will appear in the "Subject" box: "test subject"
           </li>
		<li><em>(optional)</em>captcha - true/false, use Captcha response test (Captcha module must be installed). Default is false.</li>
	</ul>
	</p>
	<?php
}

function smarty_cms_about_function_contact_form_en() {
	?>
	<p>Author: Brett Batie &lt;brett-cms@classicwebdevelopment.com&gt; &amp; Simon van der Linden &lt;ifmy@geekbox.be&gt;</p>
	<p>Version: 1.4 (20061010)</p>
	<p>
	Change History:<br/>
        <ul>
        <li>l.2 : various improvements (errors handling, etc.)</li>
        <li>1.3 : added subject_get_var parameter (by elijahlofgren)</li>
        <li>1.4 : added captcha module support (by Dick Ittmann)</li>
        </ul>
	</p>
	<?php
}

function cfsanitize($content){
	return str_replace(array("\r", "\n"), "", trim($content));
}

function validEmail($email) {
	if (!preg_match("/^([\w|\.|\-|_]+)@([\w||\-|_]+)\.([\w|\.|\-|_]+)$/i", $email)) {
		return false;
		exit;
	}
	return true;
}

?>
