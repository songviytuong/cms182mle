{foreach from=$nodelist item=node}
{if $node->parent == true or ($node->current == true and $node->haschildren == true)}
<li><a {elseif $node->current == true}
<li><a {elseif $node->haschildren == true}
<li><a {elseif $node->type == 'sectionheader' and $node->haschildren == true}
<li><span class="sectionheader">{$node->menutext}</span>{elseif $node->type == 'separator'}
<li style="list-style-type: none;"> <hr class="menu_separator" />{else}
<li><a {/if}
{if $node->type != 'sectionheader' and $node->type != 'separator'}
{if $node->target}target="{$node->target}" {/if}
href="{$node->url}"><span>{$node->menutext}</span></a>
{elseif $node->type == 'sectionheader'}
><span class="sectionheader">{$node->menutext}</span></a>
{/if}
{/foreach}
{repeat string='</li></ul>' times=$node->depth-1}
</li>