{$mod->StartTabHeaders()}
{$mod->SetTabHeader('general',$lang_general,$active_general)}
{$mod->SetTabHeader('sitedown',$lang_sitedown,$active_sitedown)}
{$mod->SetTabHeader('setup',$lang_setup,$active_setup)}
{$mod->SetTabHeader('mle',$mle_lang.mle_settings,$active_mle)}
{$mod->SetTabHeader('addmle',$mle_lang.mle_languages,$active_addmle)}
{$mod->EndTabHeaders()}
{$mod->StartTabContent()}

{$mod->StartTab('general')}
<form id="siteprefform_general" method="post" action="siteprefs.php">
<div>
  <input type="hidden" name="{$SECURE_PARAM_NAME}" value="{$CMS_USER_KEY}"/>
  <input type="hidden" name="active_tab" value="general" />
  <input type="hidden" name="editsiteprefs" value="true" />
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_sitename}</p>
  <p class="pageinput"><input type="text" class="pagesmalltextarea" name="sitename" size="30" value="{$sitename}" /></p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$lang_frontendlang}</p>
  <p class="pageinput">
    <select name="frontendlang" style="vertical-align: middle;">
       {html_options options=$languages selected=$frontendlang}
    </select>
  </p>
</div>

<div class="pageoverflow">
	<p class="pagetext">{$lang_frontendwysiwygtouse}:</p>
	<p class="pageinput">
		<select name="frontendwysiwyg">
		{html_options options=$wysiwyg selected=$frontendwysiwyg}
		</select>
	</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_nogcbwysiwyg}:</p>
  <p class="pageinput"><input type="hidden" name="nogcbwysiwyg" value="0"/><input class="pagenb" type="checkbox" value="1" name="nogcbwysiwyg" {if $nogcbwysiwyg == "1"}checked="checked"{/if} /></p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$lang_globalmetadata}:</p>
  <p class="pageinput"><textarea class="pagesmalltextarea" name="metadata" cols="80" rows="20">{$metadata}</textarea>
  </p>
</div>
{if isset($themes)}
<div class="pageoverflow">
  <p class="pagetext">{$lang_logintheme}:</p>
  <p class="pageinput">
    <select name="logintheme">
      {html_options options=$themes selected=$logintheme}
    </select>
  </p>
</div>
{/if}

<div class="pageoverflow">
  <p class="pagetext">{$lang_date_format_string}:</p>
  <p class="pageinput">
    <input class="pagenb" type="text" name="defaultdateformat" size="20" maxlength="255" value="{$defaultdateformat}"/>
    <br/>{$lang_date_format_string_help}
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_thumbnail_width}:</p>
  <p class="pageinput">
    <input class="pagenb" type="text" name="thumbnail_width" size="3" maxlength="3" value="{$thumbnail_width}"/>
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_thumbnail_height}:</p>
  <p class="pageinput">
    <input class="pagenb" type="text" name="thumbnail_height" size="3" maxlength="3" value="{$thumbnail_height}"/>
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">
    <input type="submit" name="submit" value="{$lang_submit}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
    <input type="submit" name="cancel" value="{$lang_cancel}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
  </p>
</div>
</form>
{$mod->EndTab()}


{$mod->StartTab('sitedown')}
<form id="siteprefform_sitedown" method="post" action="siteprefs.php">
<div>
  <input type="hidden" name="{$SECURE_PARAM_NAME}" value="{$CMS_USER_KEY}"/>
  <input type="hidden" name="active_tab" value="sitedown" />
  <input type="hidden" name="editsiteprefs" value="true" />
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_enablesitedown}:</p>
  <p class="pageinput"><input type="hidden" name="enablesitedownmessage" value="0"/><input class="pagenb" type="checkbox" value="1" name="enablesitedownmessage" {if $enablesitedownmessage == "1"}checked="checked"{/if}/></p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$lang_sitedownmessage}:</p>
  <p class="pageinput">{$textarea_sitedownmessage}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$lang_sitedownexcludes}:</p>
  <p class="pageinput">
     <input type="text" name="sitedownexcludes" size="50" maxlength="255" value="{$sitedownexcludes}"/>
     <br/>
     {$lang_info_sitedownexcludes}
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">
    <input type="submit" name="submit" value="{$lang_submit}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
    <input type="submit" name="cancel" value="{$lang_cancel}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
  </p>
</div>
</form>
{$mod->EndTab()}


{$mod->StartTab('setup')}
<form id="siteprefform_setup" method="post" action="siteprefs.php">
<div>
  <input type="hidden" name="{$SECURE_PARAM_NAME}" value="{$CMS_USER_KEY}"/>
  <input type="hidden" name="active_tab" value="setup" />
  <input type="hidden" name="editsiteprefs" value="true" />
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_clearcache}:</p>
  <p class="pageinput">
    <input class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" type="submit" name="clearcache" value="{$lang_clear}" />
  </p>
</div>  
<div class="pageoverflow">
  <p class="pagetext">{$lang_autoclearcache}:</p>
  <p class="pageinput">
    <input type="text" class="pagesmalltextarea"  name="auto_clear_cache_age" size="4" value="{$auto_clear_cache_age}" maxlength="4"/>
    <br/>
    {$lang_info_autoclearcache}
  </p>
</div>  

<div class="pageoverflow">
  <p class="pagetext">{$lang_global_umask}:</p>
  <p class="pageinput"><input type="text" class="pagesmalltextarea" name="global_umask" size="4" value="{$global_umask}" /></p>
</div>
{if isset($testresults)}
<div class="pageoverflow">
  <p class="pagetext">{$lang_results}</p>
  <p class="pageinput"><strong>{$testresults}</strong></p>
</div>
{/if}
<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput"><input type="submit" name="testumask" value="{$lang_test}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" /></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_urlcheckversion}:</p>
  <p class="pageinput">
    <input class="pagenb" type="text" name="urlcheckversion" size="80" maxlength="255" value="{$urlcheckversion}"/>
    <br/>{$lang_info_urlcheckversion}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$lang_clear_version_check_cache}:</p>
  <p class="pageinput"><input type="hidden" name="clear_vc_cache" value="0"/><input class="pagenb" value="1" type="checkbox" name="clear_vc_cache" {if $clear_vc_cache}checked="checked"{/if} /></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_disablesafemodewarning}:</p>
  <p class="pageinput"><input type="hidden" name="disablesafemodwarning" value="0"/><input class="pagenb" type="checkbox" value="1" name="disablesafemodewarning" {if $disablesafemodewarning}checked="checked"{/if} /></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_allowparamcheckwarnings}:</p>
  <p class="pageinput"><input type="hidden" name="allowparamcheckwarnings" value="0" /><input class="pagenb" type="checkbox" value="1" name="allowparamcheckwarnings" {if $allowparamcheckwarnings}checked="checked"{/if} /></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_admin_enablenotifications}:</p>
  <p class="pageinput"><input type="hidden" name="enablenotifications" value="0"/><input class="pagenb" type="checkbox" value="1" name="enablenotifications" {if $enablenotifications}checked="checked"{/if} /></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_basic_attributes}:</p>
  <p class="pageinput">
    <select name="basic_attributes[]" multiple="multiple" size="5">
      {html_options options=$all_attributes selected=$basic_attributes}
    </select>
    <br/>
    {$lang_info_basic_attributes}
  </p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$lang_pseudocron_granularity}:</p>
  <p class="pageinput">
    <select name="pseudocron_granularity">
    {html_options options=$pseudocron_options selected=$pseudocron_granularity}
    </select><br/>
    {$lang_info_pseudocron_granularity}
  </p>
</div>  

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">
    <input type="submit" name="submit" value="{$lang_submit}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
    <input type="submit" name="cancel" value="{$lang_cancel}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
  </p>
</div>
</form>
{$mod->EndTab()}



{$mod->StartTab('mle')}
<form id="siteprefform_mle" method="post" action="siteprefs.php">
<div>
	<input type="hidden" name="{$SECURE_PARAM_NAME}" value="{$CMS_USER_KEY}"/>
	<input type="hidden" name="active_tab" value="mle" />
	<input type="hidden" name="editsiteprefs" value="true" />
</div>

<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.force_default}:</p>
	<p class="pageinput"><input type="hidden" name="force_mle_default" value="0"/><input class="pagenb" type="checkbox" value="1" name="force_mle_default" {if $force_mle_default == "1"}checked="checked"{/if}/></p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.default_lang_fallback}:</p>
	<p class="pageinput"><input type="hidden" name="default_lang_fallback" value="0"/><input class="pagenb" type="checkbox" value="1" name="default_lang_fallback" {if $default_lang_fallback == "1"}checked="checked"{/if}/></p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.root_url_default_content}:</p>
	<p class="pageinput"><input type="hidden" name="root_url_default_content" value="0"/><input class="pagenb" type="checkbox" value="1" name="root_url_default_content" {if $root_url_default_content == "1"}checked="checked"{/if}/></p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.opacity_flags_pattern}:</p>
	<p class="pageinput"><input type="text" class="pagesmalltextarea" name="opacity_flags_pattern" size="16" maxlength="255" value="{$opacity_flags_pattern}" /></p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.opacity_flags_percent}:</p>
	<p class="pageinput"><input type="text" class="pagesmalltextarea" name="opacity_flags_percent" size="2" maxlength="2" value="{$opacity_flags_percent}" /></p>
</div>
<div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">
	<input type="submit" name="testmle_db" value="{$mle_lang.testmle_db}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
	<input type="submit" name="testmle_config" value="{$mle_lang.testmle_config}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
  </p>
</div>

<div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">
	<input type="submit" name="submit" value="{$lang_submit}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
	<input type="submit" name="cancel" value="{$lang_cancel}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
	</p>
</div>
</form>
{$mod->EndTab()}


{$mod->StartTab('addmle')}
<form id="siteprefform_addmle" method="post" action="siteprefs.php">
<div>
	<input type="hidden" name="{$SECURE_PARAM_NAME}" value="{$CMS_USER_KEY}"/>
	<input type="hidden" name="active_tab" value="addmle" />
	<input type="hidden" name="editsiteprefs" value="true" />
</div>

<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.flags_abs_path}:</p>
	<p class="pageinput"><input type="text" class="pagesmalltextarea" id="flags_abs_path" name="flags_abs_path" size="55" maxlength="255" value="{$flags_abs_path}" /></p>
</div>

{if isset($old_mle_config)}
<div class="pageoverflow">
	<p><input type="hidden" name="old_mle_config" value="1" /></p>
	<p style="color:red;"><b>{$mle_lang.old_mle_config}</b></p>
</div>
{else}
<div class="pageoverflow">
	<p class="pagetext">{$mle_lang.add_language}:</p>
	<p class="pageinput">
	<select id="mle_frontendlang" name="mle_frontendlang" style="vertical-align: middle;" onchange="update_newlanguage();">
		{html_options options=$languages selected=$frontendlang}
	</select>
	{if !isset($hls)}<br /><strong>{$mle_lang.choose_default}</strong>{/if}
	</p>
</div>
{/if}
<div id="newlanguage" style="display:none;">
	<fieldset>
	<legend><strong>{$mle_lang.new_language}</strong></legend>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.db_block}:</p>
		<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_block" name="new_block" size="3" maxlength="3" value="" />
		{$mle_lang.info_db_block}</p>
	</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.parent}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_parent" name="new_parent" size="9" maxlength="9" value="" />
		{$mle_lang.info_parent}</p>
		</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.label_url}:</p>
		<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_key" name="new_key" size="9" maxlength="9" value="" />
		{$mle_lang.info_label_url}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.flag}:</p>
		<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_flag" name="new_flag" size="9" maxlength="9" value="" />
		<img id="new_flag_img" src="{$flags_abs_path}/" />
		</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.native_text}:</p>
		<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_text" name="new_text" size="30" maxlength="60" value="" /></p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.locale}:</p>
		<p class="pageinput"><input type="text" class="pagesmalltextarea" id="new_locale" name="new_locale" size="20" maxlength="40" value="" />
		{$mle_lang.info_locale}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.default_language}:</p>
		<p class="pageinput"><input type="radio" class="pagesmalltextarea" id="default_lang_mle" name="default_lang_mle" value="" /></p>
	</div>
	{if !isset($hls)}
	<div class="pageoverflow">
		<p class="pagetext">{$mle_lang.copy_current_site}:</p>
		<p class="pageinput"><input type="hidden" name="copy_current_site" value="0" /><input class="pagenb" type="checkbox" value="1" name="copy_current_site" /></p>
	</div>
	{/if}
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">
			<input type="submit" name="addmlelanguage" value="{$mle_lang.add_language}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
		</p>
	</div>
	</fieldset>
</div>

	{if isset($hls)}
		<div class="pageoverflow">
			<h2 class="pagetext" style="font-size:1.3em;color:navy;">{$mle_lang.config_lang_editor}</h2>
			<h3 class="pagetext" style="color:teal;">
				{$mle_lang.default_lang_mle}: &nbsp;
				{if isset($missing_mle_default)}<span style="color:red;">{$mle_lang.missing_mle_default}</span>
				{else}{$default_lang_mle}
				{/if}
				{if isset($default_language_locale) && $default_language_locale!=$default_lang_mle}({$default_language_locale}){/if}
			</h3>
		</div>
		{foreach from=$hls key=key item=mle}
		<fieldset>
		<legend><strong>{$mle.locale_cms|default:''}</strong></legend>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.db_block}: &nbsp; {$mle.block}</p>
			<p><input type="hidden" name="{$key}_block" value="{$mle.block}" /></p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.parent}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" name="{$key}_parent" size="9" maxlength="9" value="{$mle.parent|default:''}" />
		{$mle_lang.info_parent}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.label_url}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" name="{$key}_key" size="9" maxlength="9" value="{$mle.key|default:$key}" />
		{$mle_lang.info_label_url_new}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.flag}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" name="{$key}_flag" size="9" maxlength="9" value="{$mle.flag|regex_replace:'/(\<img src=\")(.*?)(\" .*?)$/ism':'\2'|regex_replace:'/(.*?\/)(\w+.png)$/ism':'\2'}" />
			<img src="{$flags_abs_path}/{$mle.flag|regex_replace:'/(\<img src=\")(.*?)(\" .*?)$/ism':'\2'|regex_replace:'/(.*?\/)(\w+.png)$/ism':'\2'}" />
			</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.native_text}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" name="{$key}_text" size="30" maxlength="60" value="{$mle.text|default:''}" /></p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.locale}:</p>
			<p class="pageinput"><input type="text" class="pagesmalltextarea" name="{$key}_locale" size="20" maxlength="40" value="{$mle.locale|default:''}" />
		{$mle_lang.info_locale}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.default_language}:</p>
			<p class="pageinput"><input type="radio" class="pagesmalltextarea" name="default_lang_mle" value="{$key}" {if $default_lang_mle==$key}checked="checked"{/if} /></p>
		</div>
		{if $default_lang_mle!=$key}
		<div class="pageoverflow">
			<p class="pagetext">{$mle_lang.remove_language}:</p>
			<p class="pageinput"><input type="checkbox" class="pagesmalltextarea" name="remove_lang_mle" value="{$key}" /></p>
		</div>
		{/if}
		<div><input type="hidden" name="{$key}_locale_cms" value="{$mle.locale_cms|default:''}" /></div>
		</fieldset>
		{/foreach}
	{/if}

<div class="pageoverflow" id="mlelanguages" style="display:true;">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">
		<input type="submit" name="submit" value="{$lang_submit}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
		<input type="submit" name="cancel" value="{$lang_cancel}" class="pagebutton" onmouseover="this.className='pagebuttonhover'" onmouseout="this.className='pagebutton'" />
	</p>
</div>

{literal}
<script type='text/javascript'>
function update_newlanguage(){
	var selectedOption = $A($('mle_frontendlang').options).find(function(option) { return option.selected; } );
	var lang=selectedOption.value;
	var up_country=lang.substr(0,2);
	var country=up_country.toLowerCase();
	if(lang==''){
		$('mlelanguages').show();
		$('newlanguage').hide();
	}else{
		$('new_block').value = lang.substr(0,2);
		$('new_parent').value = lang.substr(0,2);
		$('new_key').value = lang;
		$('new_flag').value = country+'.png';
		$('new_flag_img').src = $('flags_abs_path').value+'/'+country+'.png';
		$('new_text').value = selectedOption.text;
		$('new_locale').value = lang+'.UTF-8';
		$('default_lang_mle').value = lang;
		$('mlelanguages').hide();
		$('newlanguage').show();
	}
}
</script>
{/literal}
</form>
{$mod->EndTab()}

{$mod->EndTabContent()}
