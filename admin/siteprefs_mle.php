<?php
#------------------------------------------------------------------------
# MLE - MultiLanguage Edition
# 2007 (c) alby (Alberto Benati) <cms@xme.it>
#------------------------------------------------------------------------
# CMS Made Simple is (c) 2005-2009 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#------------------------------------------------------------------------
#$Id: siteprefs_mle.php 133 2010-08-30 12:58:39Z alby $

$mle_lang['mle_settings'] = 'MLE settings';
$mle_lang['mle_languages'] = 'MLE languages';
$mle_lang['force_default'] = 'Force initial calling to DEFAULT_LANG';
$mle_lang['default_lang_fallback'] = 'Show from DEFAULT_LANG if current lang is not ready';
$mle_lang['root_url_default_content'] = 'Use root_url for Default page (there is a problem on few php-cgi installations)';
$mle_lang['opacity_flags_pattern'] = 'Opacity text replace (IE > 6 only!)';
$mle_lang['opacity_flags_percent'] = 'Opacity flags percent';
$mle_lang['testmle_db'] = 'Submit and test MLE DB';
$mle_lang['testmle_config'] = 'Submit and test MLE config';

$mle_lang['flags_abs_path'] = 'Absolute path to flags';
$mle_lang['choose_default'] = 'Choose the DEFAULT site language';
$mle_lang['new_language'] = 'New language';
$mle_lang['copy_current_site'] = 'Do you want copy current site in THIS lang?';
$mle_lang['add_language'] = 'Add new language';
$mle_lang['config_lang_editor'] = 'MLE Config Lang Web Editor';
$mle_lang['default_lang_mle'] = 'Current Default Language';
$mle_lang['missing_mle_default'] = 'MISSING DEFAULT LANGUAGE! Select your DEFAULT site language';
$mle_lang['db_block'] = 'DB block';
$mle_lang['info_db_block'] = 'It is mandatory that this field is UNIQUE compared to other languages';
$mle_lang['parent'] = 'Parent language';
$mle_lang['info_parent'] = 'Language parent (2 chars ex: en, de, it, ..) or with other same parent language use 5 chars with DASH ex: en-gb, en-us, en-au';
$mle_lang['label_url'] = 'Label URL';
$mle_lang['info_label_url'] = 'It is mandatory that this field is UNIQUE compared to other languages. In general it\'s locale';
$mle_lang['info_label_url_new'] = 'It is mandatory that this field is UNIQUE compared to other languages. Changing in production site isn\'t good for SEO';
$mle_lang['flag'] = 'Flag';
$mle_lang['native_text'] = 'Native text';
$mle_lang['locale'] = 'Locale';
$mle_lang['info_locale'] = 'Specific locale server. In general is xx_XX.UTF-8';
$mle_lang['default_language'] = 'DEFAULT site language';
$mle_lang['remove_language'] = 'Remove this language (but not remove content from DB!)';

$mle_lang['test_no_hls'] = 'MLE is not currently active!';
$mle_lang['test_db_success'] = 'MLE DB installation was successfully tested';
$mle_lang['error_db_block'] = 'block field "%s" is not UNIQUE!. This broke MLE installation!';
$mle_lang['error_db_test'] = 'Error in some DB fields:';
$mle_lang['test_config_success'] = 'MLE config sintax was successfully tested';
$mle_lang['error_config_hl'] = 'key field "%s" is not UNIQUE!. This broke MLE installation!';
$mle_lang['error_config_block'] = 'block field "%s" is not UNIQUE!. This broke MLE installation!';
$mle_lang['error_config_test'] = 'Error in some MLE config fields:';
$mle_lang['test_locale_cms_failed'] = 'locale_cms field is missing in %s! Look in README.FIRST';
$mle_lang['test_parent_failed'] = 'parent field is missing in %s! Look in README.FIRST';
$mle_lang['test_flag_failed'] = 'flag field is missing in %s! Look in README.FIRST';
$mle_lang['test_text_failed'] = 'text field is missing in %s! Look in README.FIRST';
$mle_lang['error_prop_names'] = 'updateProp_Names: error in ID# %s - %s';
$mle_lang['error_monoupdate'] = 'updateFromMonoContent: error in %s table for %s';
$mle_lang['old_mle_config'] = 'You have a OLD config_lang.php (pre 1.6). Please checks ALL values BELOW and Submit for create a correct new version of config_lang.php!';
$mle_lang['add_missing_some'] = 'Add language missing some values!';
$mle_lang['not_unique'] = 'DB block or Label URL are not UNIQUE!. Exiting!';
$mle_lang['error_add_column'] = 'Error add column or already present: %s';
$mle_lang['error_check_column'] = 'Error check column %s in %s';
$mle_lang['error_writing_dir'] = 'Your directory "%s" is not writable from php!';
$mle_lang['error_create_files'] = 'Error in create %s! If exist file this is not writable. Create a new and give full write permission';
$mle_lang['error_create_files_bak'] = 'Error in create backup of %s!';
$mle_lang['error_file_notwritable'] = 'Your file(s) %s are not writable!';
$mle_lang['writing_file_failed'] = 'Writing %s failed!';
$mle_lang['add_language_success'] = 'New language was successfully added';
$mle_lang['update_language_success'] = 'Languages was successfully updated';

//Attention to UNIQUE keys!
$mle_content_fields = array('C(165)'=>'content_name', 'C(164)'=>'menu_text', 'X'=>'metadata', 'C(163)'=>'titleattribute');
$mle_htmlblobs_fields = array('X'=>'html');
$mle_error=array();

//SETTINGS
if(isset($_POST["testmle_db"]))
{
	if(!isset($hls))
	{
		$error .= '<li>'.$mle_lang['test_no_hls'].'</li>';
	}
	else
	{
		$mle_error = ckeckMLEDB($hls, $mle_lang, false, $mle_content_fields, $mle_htmlblobs_fields);
		if(count($mle_error) == 0)
			$message .= '<br />'.$mle_lang['test_db_success'];
		else
			$error .= '<li>'.implode("</li>\n<li>", array_merge((array)$mle_lang['error_db_test'],$mle_error)).'</li>';
	}
}
else if(isset($_POST["testmle_config"]))
{
	if(!isset($hls))
	{
		$error .= '<li>'.$mle_lang['test_no_hls'].'</li>';
	}
	else
	{
		$mle_error = ckeckMLEConfig($hls, $mle_lang);
		if(count($mle_error) == 0)
			$message .= '<br />'.$mle_lang['test_config_success'];
		else
			$error .= '<li>'.implode("</li>\n<li>", array_merge((array)$mle_lang['error_config_test'],$mle_error)).'</li>';
	}
}

//LANGUAGES
else if(isset($_POST["addmlelanguage"]))
{
	if(empty($_POST["mle_frontendlang"]) || empty($_POST["new_block"]) || empty($_POST["new_key"]) || empty($_POST["flags_abs_path"]))
	{
		$error .= '<li>'.$mle_lang['add_missing_some'].'</li>';
		return;
	}

	$_flags = trim($_POST["flags_abs_path"]);
	if(substr($_flags, -1) != '/') $_flags .= '/';

	$new_key = trim($_POST["new_key"]);
	$new_locale_cms = trim($_POST["mle_frontendlang"]);
	$new_block = strtolower(trim($_POST["new_block"]));
	$new_parent = (isset($_POST["new_parent"])) ? trim($_POST["new_parent"]) : $new_block;
	$new_flag = $_flags.((isset($_POST["new_flag"])) ? trim($_POST["new_flag"]) : $new_parent.'.png');
	$new_text = (isset($_POST["new_text"])) ? trim($_POST["new_text"]) : '';
	$new_locale = (isset($_POST["new_locale"])) ? trim($_POST["new_locale"]) : $new_locale_cms;
	if($_POST["default_lang_mle"] == $new_locale_cms)
	{
		$default_lang_mle = $new_key;
		set_site_preference('default_lang_mle',$default_lang_mle);
	}

	//Check UNIQUE
	if(isset($hls))
	{
		foreach($hls as $hl=>$mle)
		{
			if($new_block==$mle['block'] || $hl==$new_key)
			{
				$error .= '<li>'.$mle_lang['not_unique'].'</li>';
				return;
			}
		}
	}

	//Add columns to content
	if(count($mle_error) == 0)
		$mle_error = addNewLangDB($new_block, $mle_lang, $mle_content_fields, $mle_htmlblobs_fields);

	//Update prop_name
	if(count($mle_error) == 0)
		$mle_error = updateProp_Names($new_block, $mle_lang);

	//Copy content
	if(isset($_POST["copy_current_site"]) && count($mle_error)==0)
		$mle_error = updateFromMonoContent($new_block, $mle_lang);

	if(count($mle_error) == 0)
	{
		$new_hls[$new_key] = array(
		'locale_cms'=>$new_locale_cms,
		'block'=>$new_block,
		'parent'=>$new_parent,
		'flag'=>'<img src="'.$new_flag.'" style="border:0;'.$opacity_flags_pattern.'" alt="'.$new_text.'" title="'.$new_text.'" />',
		'text'=>$new_text,
		'locale'=>$new_locale
		);

		if(function_exists('opacity_flags'))
			opacity_flags(get_site_preference('opacity_flags_pattern','opacity:1;'), get_site_preference('opacity_flags_percent',55), true);
		$hls = (isset($hls)) ? array_merge($hls, $new_hls) : $new_hls;
		$mle_error = saveMLEConfig($hls, $mle_lang);
	}

	if(count($mle_error) == 0)
		$message .= '<br />'.$mle_lang['add_language_success'];
	else
		$error .= '<li>'.implode("</li>\n<li>", $mle_error).'</li>';
}

else if((isset($_POST["editsiteprefs"]) || isset($_POST["remove_lang_mle"])) && $active_tab=='addmle' && isset($hls))
{
	$_flags = trim($_POST["flags_abs_path"]);
	if(substr($_flags, -1) != '/') $_flags .= '/';

	$new_hls=array();
	foreach($hls as $hl=>$mle)
	{
		if(isset($_POST["remove_lang_mle"]) && $_POST["remove_lang_mle"]==$hl) continue;

		$key = trim($_POST[$hl."_key"]);
		$locale_cms = (!empty($_POST[$hl."_locale_cms"])) ? trim($_POST[$hl."_locale_cms"]) : array_search($key, $t_hls);
		$block = strtolower(trim($_POST[$hl."_block"]));
		$parent = (!empty($_POST[$hl."_parent"])) ? trim($_POST[$hl."_parent"]) : $block;
		$flag = $_flags.((isset($_POST[$hl."_flag"])) ? trim($_POST[$hl."_flag"]) : $parent.'.png');
		$text = (!empty($_POST[$hl."_text"])) ? trim($_POST[$hl."_text"]) : '';
		$locale = (!empty($_POST[$hl."_locale"])) ? trim($_POST[$hl."_locale"]) : $locale_cms;
		if($_POST["default_lang_mle"] == $locale_cms || ($default_lang_mle == $hl && $key != $default_lang_mle))
		{
			$default_lang_mle = $key;
			set_site_preference('default_lang_mle',$default_lang_mle);
		}

		$new_hls[$key] = array(
		'locale_cms'=>$locale_cms,
		'block'=>$block,
		'parent'=>$parent,
		'flag'=>'<img src="'.$flag.'" style="border:0;'.$opacity_flags_pattern.'" alt="'.$text.'" title="'.$text.'" />',
		'text'=>$text,
		'locale'=>$locale
		);
	}

	if(isset($old_mle_config))
	{
		$mle_error = ckeckMLEDB($new_hls, $mle_lang, true, $mle_content_fields, $mle_htmlblobs_fields);
		if(count($mle_error) > 0)
		{
			$error .= '<li>'.implode("</li>\n<li>", $mle_error).'</li>';
			return;
		}
	}

	if(count($mle_error) == 0)
	{
		$hls = $new_hls;
		$mle_error = saveMLEConfig($hls, $mle_lang);
	}

	if(count($mle_error) == 0)
	{
		$smarty->assign('old_mle_config',null);
		$message .= '<br />'.$mle_lang['update_language_success'];
	}
	else
	{
		$error .= '<li>'.implode("</li>\n<li>", $mle_error).'</li>';
	}
}




//FUNCTIONS
function ckeckMLEConfig($hls, $mle_lang)
{
	$error=array();
	$arr_key=array();
	$arr_block=array();
	foreach($hls as $hl=>$mle)
	{
		if(empty($mle['locale_cms'])) $error[] = sprintf($mle_lang['test_locale_cms_failed'],$hl);
		if(empty($mle['parent'])) $error[] = sprintf($mle_lang['test_parent_failed'],$hl);
		if(empty($mle['flag'])) $error[] = sprintf($mle_lang['test_flag_failed'],$hl);
		if(empty($mle['text'])) $error[] = sprintf($mle_lang['test_text_failed'],$hl);

		if(in_array($hl, $arr_key)) $error[] = sprintf($mle_lang['error_config_hl'], $hl);
		if(in_array($mle['block'], $arr_block)) $error[] = sprintf($mle_lang['error_config_block'], $mle['block']);
		$arr_key[] = $hl;
		$arr_block[] = $mle['block'];
	}
	return $error;
}

function ckeckMLEDB($hls, $mle_lang, $add=false, $mle_content_fields=array(), $mle_htmlblobs_fields=array())
{
	$error=array();
	$arr_block=array();
	foreach($hls as $hl=>$mle)
	{
		$err = _loop_mle_tables($mle['block'], $mle_lang, $add, $mle_content_fields, $mle_htmlblobs_fields);
		if(count($err) > 0) $error = array_merge($error, $err);

		if(in_array($mle['block'], $arr_block)) $error = array_merge($error, (array)sprintf($mle_lang['error_db_block'], $mle['block']));
		$arr_block[] = $mle['block'];
	}
	return $error;
}

function addNewLangDB($new_block, $mle_lang, $mle_content_fields=array(), $mle_htmlblobs_fields=array())
{
	return _loop_mle_tables($new_block, $mle_lang, true, $mle_content_fields, $mle_htmlblobs_fields);
}

function updateProp_Names($block, $mle_lang)
{
	global $gCms;
	$db = &$gCms->GetDb();

	$error=array();
	$arr_block[] = 'content_'.$block;

	$q = "SELECT content_id, prop_names	FROM ".cms_db_prefix()."content";
	$dbresult = $db->Execute($q);
	while($dbresult && $row=$dbresult->FetchRow())
	{
		$arr_props = explode(',', $row['prop_names']);
		$unique_arr_props = array_unique(array_merge($arr_props, $arr_block));
		$qu = "UPDATE ".cms_db_prefix()."content SET prop_names = ? WHERE content_id = ?";
		$dbresult_u = $db->Execute($qu, array(implode(',',$unique_arr_props), $row['content_id']));
		if(!$dbresult_u)
		{	//Restore
			$qu = "UPDATE ".cms_db_prefix()."content SET prop_names = ? WHERE content_id = ?";
			$dbresult_u = $db->Execute($qu, array($row['prop_names'], $row['content_id']));
			$error[] = sprintf($mle_lang['error_prop_names'], $row['content_id'], $row['prop_names']);
			break;
		}
	}
	return $error;
}

function updateFromMonoContent($block, $mle_lang)
{
	global $gCms;
	$db = &$gCms->GetDb();

	$error=array();
	$qu = "UPDATE ".cms_db_prefix()."content SET
		content_name_{$block} = content_name,
		menu_text_{$block} = menu_text,
		metadata_{$block} = metadata,
		titleattribute_{$block} = titleattribute";
	$dbresult = $db->Execute($qu);
	if(!$dbresult) $error[] = sprintf($mle_lang['error_monoupdate'], 'content', $block);

	$qu = "UPDATE ".cms_db_prefix()."htmlblobs SET
		html_{$block} = html";
	$dbresult = $db->Execute($qu);
	if(!$dbresult) $error[] = sprintf($mle_lang['error_monoupdate'], 'htmlblobs', $block);

	if(count($error)==0 && $block!='en')
	{
		$qi = "INSERT INTO ".cms_db_prefix()."content_props
			(content_id,type,prop_name,param1,param2,param3,content,create_date,modified_date)
			SELECT f.content_id, f.type, 'content_{$block}', f.param1, f.param2, f.param3, f.content, f.create_date, f.modified_date
				FROM ".cms_db_prefix()."content_props AS f WHERE f.prop_name = 'content_en' AND f.type = 'string'";
		$dbresult = $db->Execute($qi);
		if(!$dbresult) $error[] = sprintf($mle_lang['error_monoupdate'], 'content_props', $block);
	}
	return $error;
}

function saveMLEConfig($arr_hls, $mle_lang)
{
	$error=array();
	$filedir = dirname(CONFIG_FILE_LOCATION);
	$newfilename = dirname(CONFIG_FILE_LOCATION).DIRECTORY_SEPARATOR.'config_lang.php';

	if(!is_writable($filedir))
		$error[] = sprintf($mle_lang['error_writing_dir'], $filedir);

	if(count($error) == 0)
	{
		if(!file_exists($newfilename))
		{
			if(!touch($newfilename)) $error[] = sprintf($mle_lang['error_create_files'], $newfilename);
		}
		elseif(is_writable($newfilename))
		{
			//Try to backup
			$bakupg=false;
			$oldconfig = file_get_contents($newfilename);
			$handle = fopen($newfilename .'.bakupg', "w");
			if($handle)
			{
				$bakupg = fwrite($handle, $oldconfig);
				fclose($handle);
			}
			if($bakupg === false) $error[] = sprintf($mle_lang['error_create_files_bak'], $newfilename);
		}
		else
		{
			$error[] = sprintf($mle_lang['error_file_notwritable'], $newfilename);
		}
	}

	if(count($error) == 0)
	{
		$handle = fopen($newfilename, "wb");
		if($handle)
		{
			$test = fwrite($handle, "<?php\n". _mle_config_text($arr_hls) ."?>");
			fclose($handle);

			if(!$test)
			{
				if(isset($bakupg))
				{
					//Failed, restore old config file (usefull in upgrade)
					copy($newfilename .'.bakupg', $newfilename);
					unlink($newfilename .'.bakupg');
				}
				$error[] = sprintf($mle_lang['writing_file_failed'], $newfilename);
			}
			else
			{
				if(isset($bakupg)) unlink($newfilename .'.bakupg');
			}
		}
	}
	return $error;
}


//PRIVATE
function _loop_mle_tables($block, $mle_lang, $add=false, $mle_content_fields=array(), $mle_htmlblobs_fields=array())
{
	$error=array();
	foreach($mle_content_fields as $schema=>$field)
	{
		$err = _check_addfield_table("{$field}_{$block}", 'content', $mle_lang, $add, $schema);
		if(false !== $err) $error[] = $err;
	}
	foreach($mle_htmlblobs_fields as $schema=>$field)
	{
		$err = _check_addfield_table("{$field}_{$block}", 'htmlblobs', $mle_lang, $add, $schema);
		if(false !== $err) $error[] = $err;
	}
	return $error;
}
function _check_addfield_table($field, $table, $mle_lang, $add=false, $schema='')
{
	global $gCms;
	$db = &$gCms->GetDb();
	$dbdict = NewDataDictionary($db);

	$error=false;
	$dbresult =& $db->Execute("SELECT {$field} FROM ".cms_db_prefix().$table);
	if(!$dbresult)
	{
		if($add && !empty($schema))
		{
			$c = $dbdict->AddColumnSQL(cms_db_prefix().$table, $field.' '.$schema);
			$return = $dbdict->ExecuteSQLArray($c);
			if($return <> 2) $error = sprintf($mle_lang['error_add_column'], $c[0]);
		}
		else
		{
			$error = sprintf($mle_lang['error_check_column'], $field, cms_db_prefix().$table);
		}
	}
	return $error;
}
function _mle_config_text( $arr_hls )
{
	$result = <<<EOF

/*******
 SINTAX:
 'KEY' => array(
	'locale_cms' => 'LOCALE LANGUAGE IN CMSMS', //MANDATORY One valid CMSMS locale or near to
	'block' => 'DATABASE KEY', //MANDATORY AND UNIQUE In general 2 chars same of 'parent'
	'parent' => 'PARENT LANGUAGE', //OPTIONAL 2 chars parent language, if you have others set to 5 WITH DASH, ex: en-au
	'flag' => 'HTML IMG TAG FOR LOCALE FLAG', //MANDATORY ABSOLUTE URL PATH
	'text' => 'NATIVE LANGUAGE', //OPTIONAL Use for lang plugin if you don't use flags icon
	'locale' => 'SERVER LOCALE', //OPTIONAL Set for specific locale server if different from 'locale_cms'
 ),
 *******/

\$hls = array(

EOF;
	foreach($arr_hls as $hl=>$mle)
	{
		$result .= <<<EOF
 '{$hl}' => array(
	'locale_cms'=>'{$mle['locale_cms']}',
	'block'=>'{$mle['block']}',
	'parent'=>'{$mle['parent']}',
	'flag'=>'{$mle['flag']}',
	'text'=>'{$mle['text']}',
	'locale'=>'{$mle['locale']}'
 ),

EOF;
	}
	$result .= <<<EOF
);

EOF;
	return $result;
}
?>
